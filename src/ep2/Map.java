package ep2;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.util.ArrayList;
import java.util.Random;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.Timer;

import sun.java2d.pipe.DrawImage;

public class Map extends JPanel implements ActionListener {
	
	private static final int SCORE_M = 250;
	private static final int SCORE_H = 750;
	
	private static final int EASY = 5;
	private static final int MEDIUM = 10;
	private static final int HARD = 15;
	private static final int DELAY = 80;
	private int delay=0;
	
	private final Image background;
    private final Spaceship spaceship;
    private ArrayList<Alien> alien;
    private int difficulty;
    private boolean inGame; 
    
    private final int SPACESHIP_X = 400;
    private final int SPACESHIP_Y = 430;
    private final Timer timer_map;
    private JLabel score; 
	
	private static final String EASYSTRING = "images/alien_EASY.png";
	private static final String MEDIUMSTRING = "images/alien_MEDIUM.png";
	private static final String HARDSTRING = "images/alien_HARD.png";
	private String Image_Difficulty;
	
	private MusicPlayer music;
    
    private final Image explosion;
    
    private enum STATE{
    	MENU,
    	GAME
    };
    

    public Map() {
        
        addKeyListener(new KeyListerner());
        inGame = true;
        setFocusable(true);
        setDoubleBuffered(true);
        ImageIcon image = new ImageIcon("images/earth.jpg");
        music = new MusicPlayer("theme.wav");
        ImageIcon explosio = new ImageIcon("images/explosion.png");
        explosion = explosio.getImage();
        
        this.background = image.getImage();
        
        difficulty = EASY;
        Image_Difficulty = EASYSTRING;

        spaceship = new Spaceship(SPACESHIP_X, SPACESHIP_Y);
        alien = new ArrayList<Alien>();
       
        score = new JLabel("Score = "+spaceship.getScore());
        score.setBounds(0, 0, 50, 10);
        score.setForeground(Color.cyan);

        add(score);

        timer_map = new Timer(Game.getDelay(), this);
        timer_map.start();
        music.play();
                            
    }
    
    @Override
    public void paintComponent(Graphics g) {
    	
        super.paintComponent(g);
        g.drawImage(this.background, 0, 0, null);
           	
    	if(inGame){
    		
    		drawObjects(g);
    		
    	}
    	
    	else {
    		
    		drawGameOver(g);
    	}
        
        Toolkit.getDefaultToolkit().sync();
    }

    private void drawObjects(Graphics g) {
        
        drawSpaceship(g);
        drawAlien(g);
        drawScore(g);
	}

	private void drawAlien(Graphics g) {
		
		for(Alien alien : alien){
			
			if(alien.isVisible())
				
				g.drawImage(alien.getImage(), alien.getX(), alien.getY(), this);
			

		}
	}

	private void drawSpaceship(Graphics g) {

        g.drawImage(spaceship.getImage(), spaceship.getX(), spaceship.getY(), this);
        
        for(Missile mis: spaceship.getMissile()){
        	
        	g.drawImage(mis.getImage(), mis.getX(), mis.getY(), this);
        }
        
    }
    
	private void drawScore(Graphics g){
		
	}
	
    public void actionPerformed(ActionEvent e) {
    	
    	spaceshipColide();
    	alienColide();
    	
    	updateAlien();
    	updateScore();
        updateSpaceship();
        updateDifficulty();
        
        isInGame();

        if(delay==DELAY){
        	generateAlien();        	
            delay=0;
        }
        else
        	++delay;
  
        repaint();
    }
    

	private void updateScore() {
		
		score.setText("Score = "+spaceship.getScore());
		
	}

	private void updateDifficulty() {
		
		if(spaceship.getScore()>SCORE_H){
			
			difficulty = HARD;
			Image_Difficulty = HARDSTRING;
			
		} 
		else if(spaceship.getScore()>SCORE_M){
			
			difficulty = MEDIUM;
			Image_Difficulty = MEDIUMSTRING;
		}
		
	}

	private void isInGame(){
		
		if(!inGame){
			
			timer_map.stop();
		}
		
	}
	
	private synchronized void alienColide() {
		
		ArrayList<Missile> missile = spaceship.getMissile();
		
		for(int i=0;i<alien.size();++i){
			
			Rectangle mob = alien.get(i).getBounds();
			
			for(int j=0; j< missile.size();++j){
				
				Rectangle mis = missile.get(j).getBounds();
				
				if(mob.intersects(mis)){
					
					alien.get(i).setVisible(false);
					missile.get(j).setVisible(false);
					spaceship.updateScore(difficulty);
				}
			}
		}
		spaceship.removeMissile();	
	}

	private synchronized void spaceshipColide() {
		
		Rectangle spaceShip = spaceship.getBounds();
		
		for(int i=0;i<alien.size();++i){
			
			Rectangle mob = alien.get(i).getBounds();
			
			if(spaceShip.intersects(mob)){
				
				alien.get(i).setVisible(false);
				inGame = false;
				music.stop();
			}
		}		
	}

	private synchronized void updateAlien() {

		for(int i=0;i<alien.size();++i) {
			
			if(alien.get(i).getY() >Game.getHeight() || !alien.get(i).isVisible()) {
				
				alien.remove(i);
			}
			
			else
				alien.get(i).update();
		}
	}

	private synchronized void generateAlien() {
		
		Random randomAlien = new Random();
		
		int[] aliensXPosition = new int[difficulty];
		
		for(int i=0;i<difficulty;++i){
			
			int mableX = randomAlien.nextInt(Game.getWidth());
			
			for(int j=0; j<i;++j){
				
				if(mableX == aliensXPosition[j]) {
					mableX = randomAlien.nextInt(Game.getWidth());
					j=-1;
				}
			}
			
			aliensXPosition[i]=mableX;
		}
		
		for (int i = 0; i < aliensXPosition.length; i++) {
			
			alien.add(new Alien(aliensXPosition[i], 0, Image_Difficulty));
		}

	}
    
    private void drawGameOver(Graphics g) {

        String message = "Game Over";
        Font font = new Font("arial", Font.BOLD, 20);
        FontMetrics metric = getFontMetrics(font);
        
        score.setFont(font);
        score.setBounds((Game.getWidth() - metric.stringWidth("Score = "+spaceship.getScore()))/2, (Game.getHeight() / 2)+1, 400, 20);
        g.setColor(Color.cyan);
        g.setFont(font);
        g.drawString(message, (Game.getWidth() - metric.stringWidth(message)) / 2, Game.getHeight() / 2);
    }
    
    
    private synchronized void updateSpaceship() {
    	
        spaceship.move();
        spaceship.updateMissile();
        
    }
  

    private class KeyListerner extends KeyAdapter {
        
        @Override
        public void keyPressed(KeyEvent e) {
            spaceship.keyPressed(e);
        }

        @Override
        public void keyReleased(KeyEvent e) {
            spaceship.keyReleased(e);
        }

        
    }
}