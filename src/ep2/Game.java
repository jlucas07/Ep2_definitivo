package ep2;

public class Game {
	
	private static final int WIDTH = 800;
    private static final int HEIGHT = 500;
    private static final int DELAY = 10;
    
    public static int getWidth(){
        return WIDTH;
    }
    
    public static int getHeight(){
        return HEIGHT;
    }
    
    public static int getDelay(){
        return DELAY;
    }

}
