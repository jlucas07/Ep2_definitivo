package ep2;
import java.applet.Applet;
import java.applet.AudioClip;
import java.net.URL;


public class MusicPlayer implements Runnable {

	private AudioClip audio;

	public MusicPlayer(String music) {
		URL url = MusicPlayer.class.getResource(music);
		this.audio = Applet.newAudioClip(url);
	}

	public void play() {
		
		audio.play();
	}

	@Override
	public void run() {
		
			loop();
	}
	
	public void stop() {
		
		audio.stop();
	}

	public void loop() {
		
		audio.loop();
	}

}