package ep2;
import java.awt.EventQueue;
import javax.swing.JFrame;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Timer;

public class Application extends JFrame{

	public Application() {
		add(new Map());

        setSize(Game.getWidth(), Game.getHeight());
        setTitle("ENDLESS SPACE BATTLE");
        setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
    }
	
	
    public static void main(String[] args) {
        
        EventQueue.invokeLater(new Runnable() {
        	
            @Override
            public void run() {
                Application app = new Application();
                app.setVisible(true);
            }
        });
        
    }
    
	
}

