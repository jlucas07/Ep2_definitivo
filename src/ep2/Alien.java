package ep2;
public class Alien extends Sprite {
	
	private static final int speed_y = 1;
	private String alien_type;

	
	public Alien (int x, int y) {
		
		super(x, y);
		alien_type = "images/alien_EASY.png";
		visible = true;
	}
	
	public Alien (int x, int y, String type) {
		
		super(x, y);
		
		this.alien_type = type;
		visible = true;
	}

	public void update(){
		
		loadImage(alien_type);
		y+=speed_y;
	}
}
