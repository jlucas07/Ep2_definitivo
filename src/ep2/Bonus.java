package ep2;
public class Bonus extends Sprite {
	
	private static final int speed_b = 1;
	private boolean isBonus;

	public Bonus(int x, int y, String type) {
		super(x, y);
		loadImage(type);
		visible = true;
	}
	
	public synchronized void explosion() {
        loadImage("images/explosion.png");
        isBonus = true;
    }

    public void movementBonus() {
        this.y += speed_b;
        if ((y + width >= Game.getHeight())) {
            visible = false;
        }
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
