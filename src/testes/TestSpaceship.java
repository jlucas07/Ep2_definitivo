package testes;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import ep2.Spaceship;

public class TestSpaceship {
	Spaceship spaceship1;
	Spaceship spaceship2;

	@Before
	public void setUpTrue() {
		spaceship1 = new Spaceship(400, 300);
	}
	
	@Before
	public void setUpFalse() {
		spaceship2 = new Spaceship(400, 300);
	}
	
	@Test
	public void testVisibleTrue() {
		spaceship1.setVisible(true);
		assertTrue(spaceship1.isVisible());
	}
	
	@Test
	public void testVisibleFalse(){
		
		spaceship2.setVisible(false);
		assertFalse(spaceship2.isVisible());
	}

}
