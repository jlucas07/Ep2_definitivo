package testes;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import ep2.Application;

public class TestApplication {
	
	Application app1;
	Application app2;
	
	@Before
	public void setUpTrue() {
		app1 = new Application();
	}

	@Test
	public void testTrue() {
		app1.setVisible(true);
		assertTrue(app1.isVisible());
	
	}
	
	@Before
	public void setUpFalse() {
		app2 = new Application();
	}

	@Test
	public void testFalse() {
		app2.setVisible(false);
		assertFalse(app2.isVisible());
	
	}

}
