ENDLESS SPACE BATTLE

O exercício prático orientado a objetos 2, situa-se como um jogo de endless run,
onde você jogador ficará jogando até ser derrotado.

Regras do jogo:

- Há apenas uma vida, então tenha cuidado;
- Os aliens são definidos em três categorias sendo elas:
	Fácil: Pontuação 5 pontos;
	Médio: Pontuação 10 pontos;
	Difícil: Pontuação 15 pontos;

Como Jogar:

As teclas Direcionais do teclado (cima, baixo, esquerda, direita) são os controles de movimento da nave.
A tecla ESPAÇO atira o míssel.


Créditos

João Lucas Sousa Reis
160009758

Música feita por: 8 Bit Universe. Rick & Morty Theme [8 Bit Tribute to Rick & Morty] - 8 Bit Universe . 1'32". Disponível em: <https://www.youtube.com/watch?v=eOGvvgY_7lA>. Acesso em junho de 2017.
